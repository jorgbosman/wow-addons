local C = {}

--
-- Configuration options
--

C["guildbank"] = {
    ["guildname"] = {"Power of One", "The Power of One"},
    ["exclude"] = {"Cyrbank"},
    ["deposit"] = {
        -- tab 1
        {"Tough Hunk of Bread"},
        -- tab 2
        {"Ice Cold Milk"},
        -- tab 3
        {"Goldenbark Apple"},
        -- tab 4
        {"Tel'Abim Banana"},
    },
    ["withdraw"] = {
        ["Tough Hunk of Bread"] = 20,
        ["Tel'Abim Banana"] = 20,
        ["Goldenbark Apple"] = 20,
        ["Ice Cold Milk"] = 20,
    },
}

--
--
--

function insanity_config()
    return C
end

