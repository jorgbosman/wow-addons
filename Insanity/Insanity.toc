## Interface: 60000
## Title : Insanity
## Notes: Insanity: doing the same thing over and over again and expecting different results. -Albert Einstein
## Author: Cyrberus
## Version 6.0.3.1

Config.lua
Insanity.lua
Insanity.xml
