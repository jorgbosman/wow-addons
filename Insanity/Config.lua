local C = {}

--
-- Configuration options
--

C["guildbank"] = {
    ["guildname"] = {"Power of One"},
    ["exclude"] = {"Cyrbank", "C�r"},
    ["deposit"] = {
        -- Tab 1 (Herbs)
        {},
        -- Tab 2 (Ore)
        {},
        -- Tab 3 (Enchanting)
        {"Fractured Temporal Crystal", "Temporal Crystal"},
        -- Tab 4 (Cloth/Leather)
        {},
        -- Tab 5 (Crafting Reagents)
        {"Savage Blood", "Sorcerous Air", "Sorcerous Water", "Sorcerous Earth", "Sorcerous Fire", "Hexweave Bag"},
        -- Tab 6 (Random junk)
        {},
    },
    ["withdraw"] = {
    },
}


--
--
--

function insanity_config()
    return C
end

