--
-- Just trying to automate the boring parts of wow
--
-- cyrberus@cyrberus.eu - 2015-02-12
--

local WAIT_TIME = 0.5

local C = insanity_config()
local timer = WAIT_TIME
local queue = {}
local guild_bank_initialized = false
local current_guild_bank_tab = nil


--
-- helper functions
--

local function queue_add(x)
    for i, item in next, queue do
        if item == x then
            return
        end
    end
    table.insert(queue, x)
end

local function queue_remove(x)
    new_queue = {}
    for i, item in next, queue do
        if not item == x then
            table.insert(new_queue, x)
        end
    end
    queue = new_queue
end

local function remember_initial_guild_bank_tab()
    if not initial_guild_bank_tab then
        initial_guild_bank_tab = GetCurrentGuildBankTab()
    end
end

local function restore_initial_guild_bank_tab()
    if initial_guild_bank_tab then
        SetCurrentGuildBankTab(initial_guild_bank_tab)
        initial_guild_bank_tab = nil
    end
end

--
-- actual functions
--

local function deposit_items(items)
    --print("Start depositing");

    local did_anything = false
    for index, tab in next, items do
        -- print("Switching to GuildBankTab "..index);
        if did_anything then return end
        SetCurrentGuildBankTab(index)
        for bag=0, 4 do
            for slot=1, GetContainerNumSlots(bag) do
                local bag_item=GetContainerItemLink(bag, slot)
                for index, item in next, items[index] do
                    if bag_item and strfind(bag_item, item) then
                        --print("Depositing "..bag_item)
                        UseContainerItem(bag, slot)
                        did_anything = true
                    end
                end
            end
        end
    end

    if not did_anything then
        print("Deposited everything i had")
        queue_remove('deposit')
        restore_initial_guild_bank_tab()
    end
end


local function withdraw_items(items)
    --print("Start withdrawing");

    local did_anything = false
    GuildBankFrame_UpdateTabs()
    for tab=1, GetNumGuildBankTabs() do
        if did_anything then return end
        SetCurrentGuildBankTab(tab)
        QueryGuildBankTab(tab)
        for slot=1, 98 do
            local bank_item=GetGuildBankItemLink(tab, slot)
            for item, amount in next, items do
                if bank_item and strfind(bank_item, item) then
                    --print("Withdrawing "..bank_item)
                    _G[format("GuildBankColumn%dButton%d", ceil(slot/14),slot%14)]:Click("RightButton")
                    did_anything = true
                end
            end
        end
    end

    if not did_anything then
        print("Withdrew everything i need")
        queue_remove('withdraw')
        restore_initial_guild_bank_tab()
    end
end


local function guild_bank_init()
    enable = false
    local unit_name, _ = UnitName("player")
    local guild_name, _, _ = GetGuildInfo("player");
    for i, name in next, C["guildbank"]["guildname"] do
        if guild_name == name then
          enable = true
        end
    end

    for i, name in next, C["guildbank"]["exclude"] do
        if unit_name == name then
          enable = false
        end
    end

    if enable then
        print("Insanity activated for "..unit_name);
        guild_bank_initialized = true

        SLASH_DEPOSIT1 = '/deposit'
        SLASH_WITHDRAW1 = '/withdraw'
    else
        print("Insanity not loaded for "..unit_name)
    end
end


--
-- lib stuff
--

frame = CreateFrame("Frame")
frame:SetScript("OnEvent", function(self, event, ...)
    self[event](self, ...)
end)
--frame:SetScript("OnShow", function(self)
--    self.elapsed = 0
--end)

frame:SetScript("OnUpdate", function(self, elapsed)
    if #queue == 0 then return end
    timer = timer + elapsed
    if timer < WAIT_TIME then
        return
    end
    --print('Finished waiting... '..timer)
    timer = 0

    for i, value in next, queue do
        if value == 'deposit' then
            deposit_items(C["guildbank"]["deposit"])
        elseif value == 'withdraw' then
            withdraw_items(C["guildbank"]["withdraw"])
        end
    end
end)

--frame:UnregisterAllEvents()
--frame:Hide()


function frame:GUILDBANKFRAME_OPENED()
    if not guild_bank_initialized then
        guild_bank_init()
    end

    if guild_bank_initialized then
        remember_initial_guild_bank_tab()
        queue_add('deposit')
    end
end


function SlashCmdList.DEPOSIT(msg, editbox)
    remember_initial_guild_bank_tab()
    queue_add('deposit')
end

function SlashCmdList.WITHDRAW(msg, editbox)
    remember_initial_guild_bank_tab()
    queue_add('withdraw')
end


--
-- main
--

frame:RegisterEvent("GUILDBANKFRAME_OPENED")

