## Interface: 40200
## Title: Grid2 |cd4ff7d0aFreeLayout|r
## Notes: Layout Manager for Grid2
## Notes-esES: Gestor de diseños para Grid2
## Author: Michael
## Dependencies: Grid2
## Version: 0.1
## X-Curse-Packaged-Version: r6
## X-Curse-Project-Name: Grid2FreeLayout
## X-Curse-Project-ID: grid2freelayout
## X-Curse-Repository-ID: wow/grid2freelayout/mainline

Grid2FreeLayout.lua
